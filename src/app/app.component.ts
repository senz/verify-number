import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'verify-number';

  isValue : boolean = false
  number : number = 0
  typeNumber : string = ''

  constructor(){}
  
  ngOnInit(): void {
  }

  getNumber(numberParams : number){
    // console.log(event)
    this.number = numberParams
    this.checkNumber(this.number , this.typeNumber)
  }

  getValueDropdown(event : string){

    this.typeNumber = event
    this.checkNumber(this.number , this.typeNumber)
  }

  isPrime(num : number){
  for(var i = 2; i < num; i++)
    if(num % i === 0) return false;
      return num > 1;
  }

  isFibonanci(num : number , count = 1 , last = 0) : boolean{
    if(count < num){
      return this.isFibonanci(num, count+last, count);
   };
   if(count === num){
      return true;
   }
   return false;
  }

  checkNumber(number : number , typeNumber : string) {
    if (typeNumber == "isPrime"){
      this.isValue = this.isPrime(number)
    }  else {
      this.isValue = this.isFibonanci(number)
    }
  }


}
