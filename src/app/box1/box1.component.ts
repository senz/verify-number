import { Component, OnInit , Input, SimpleChanges, OnChanges, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-box1',
  templateUrl: './box1.component.html',
  styleUrls: ['./box1.component.css']
})
export class Box1Component implements OnInit {

  @Output() number: EventEmitter<number> = new EventEmitter();
  numberDefault : number = 0
  constructor() { 
  }

  ngOnInit(): void {
    // console.log(this.number)
  }

  onUpdateNumber(event: Event) {
    let number = Number((<HTMLInputElement>event.target).value)
    if (number < 0){
      this.numberDefault = 1
      number = 1
    } 
    this.number.emit(Math.ceil(number))
  }

}
