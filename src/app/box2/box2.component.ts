import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-box2',
  templateUrl: './box2.component.html',
  styleUrls: ['./box2.component.css']
})
export class Box2Component implements OnInit {

  @Output() valueDropdown : EventEmitter<string> = new EventEmitter()
  constructor() { }

  ngOnInit(): void {
  }

  chageDropdown(event : Event){
    this.valueDropdown.emit((<HTMLInputElement>event.target).value)
  }

}
